const std = @import("std");
const testing = std.testing;

pub const AsciiAlphaLower = Codec(u5, ascii_alpha_lower_charmap);
pub const AsciiAlphaUpper = Codec(u5, ascii_alpha_upper_charmap);
pub const AsciiAlpha = Codec(u6, ascii_alpha_charmap);
pub const Katakana = Codec(u7, katakana_charmap);

const CodePoint = u21;

fn mapSize(comptime T: type) comptime_int {
    return std.math.maxInt(T) + 1;
}

pub fn Codec(
    comptime PackedT: type,
    comptime charmap: [mapSize(PackedT)]CodePoint,
) type {
    return struct {
        pub fn encode(codepoint: CodePoint) PackedT {
            inline for (charmap, 0..) |cp, i| {
                if (cp == codepoint)
                    return i;
            }

            @panic("encode error");
        }

        pub fn decode(c: PackedT) CodePoint {
            inline for (charmap, 0..) |cp, i| {
                if (i == c)
                    return cp;
            }

            @panic("decode error");
        }

        pub fn bits() u16 {
            return @bitSizeOf(PackedT);
        }
    };
}

const ascii_alpha_charmap = [mapSize(u6)]CodePoint{
    'A', 'B', 'C', 'D',
    'E', 'F', 'G', 'H',
    'I', 'J', 'K', 'L',
    'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T',
    'U', 'V', 'W', 'X',
    'Y', 'Z', 'a', 'b',
    'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j',
    'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r',
    's', 't', 'u', 'v',
    'w', 'x', 'y', 'z',
    ' ', '-', '_', '\'',
    '.', ',',
    // uhh dunno what else
    '+', '!',
    '@', '?', '/', '\\',
};

const ascii_alpha_lower_charmap = [mapSize(u5)]CodePoint{
    'a', 'b',  'c', 'd',
    'e', 'f',  'g', 'h',
    'i', 'j',  'k', 'l',
    'm', 'n',  'o', 'p',
    'q', 'r',  's', 't',
    'u', 'v',  'w', 'x',
    'y', 'z',  ' ', '-',
    '_', '\'', '.', ',',
};

const ascii_alpha_upper_charmap = [mapSize(u5)]CodePoint{
    'A', 'B',  'C', 'D',
    'E', 'F',  'G', 'H',
    'I', 'J',  'K', 'L',
    'M', 'N',  'O', 'P',
    'Q', 'R',  'S', 'T',
    'U', 'V',  'W', 'X',
    'Y', 'Z',  ' ', '-',
    '_', '\'', '.', ',',
};

const katakana_charmap = [mapSize(u7)]CodePoint{
    // https://en.wikipedia.org/wiki/Katakana#Syllabary_and_orthography
    'ア', 'イ', 'ウ', 'エ', 'オ',
    'カ', 'キ', 'ク', 'ケ', 'コ',
    'ガ', 'ギ', 'グ', 'ゲ', 'ゴ',
    'サ', 'シ', 'ス', 'セ', 'ソ',
    'ザ', 'ジ', 'ズ', 'ゼ', 'ゾ',
    'タ', 'チ', 'ツ', 'テ', 'ト',
    'ダ', 'ヂ', 'ヅ', 'デ', 'ド',
    'ナ', 'ニ', 'ヌ', 'ネ', 'ノ',
    'ハ', 'ヒ', 'フ', 'ヘ', 'ホ',
    'バ', 'ビ', 'ブ', 'ベ', 'ボ',
    'パ', 'ピ', 'プ', 'ペ', 'ポ',
    'マ', 'ミ', 'ム', 'メ', 'モ',
    'ヤ', 'ユ', 'ヨ', 'ラ', 'リ',
    'ル', 'レ', 'ロ', 'ワ', 'ヰ',
    'ヱ', 'ヲ', 'ン',
    // functional marks and diacritics
    'ッ', 'ヽ',
    '゛', '゜',
    // bonus stuffing
    'a',   'b',   'c',
    'd',   'e',   'f',   'g',   'h',
    'i',   'j',   'k',   'l',   'm',
    'n',   'o',   'p',   'q',   'r',
    's',   't',   'u',   'v',   'w',
    'x',   'y',   'z',   ' ',   '-',
    '_',   '\'',  '.',   ',',   ' ',
    '-',   '_',   '\'',  '.',   ',',
    '0',   '1',   '2',   '3',   '4',
    '5',   '6',   '7',   '8',   '9',
    '+',   '/',   '\\',
};
