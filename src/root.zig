const std = @import("std");
const testing = std.testing;

pub const codec = @import("codec.zig");

/// Smol string.
/// Currently only supports lower-case ascii alpha characters.
pub fn SmolStr(
    comptime capacity: usize,
    comptime decco: anytype,
) type {
    const PackedT = std.meta.Int(.unsigned, decco.bits());
    const IntT = std.meta.Int(.unsigned, std.math.log2(capacity) + 1);
    const optimize_len = @bitSizeOf(IntT) <= @bitSizeOf(PackedT);
    const LenT = if (optimize_len) PackedT else IntT;
    const len_adjust = if (optimize_len) 1 else 0;

    return struct {
        pub const Error = error{
            Overflow,
        };

        data: std.PackedIntArray(PackedT, capacity + len_adjust),
        len: if (optimize_len) void else LenT,

        const Self = @This();

        pub fn init(bytes: []const u8) Error!Self {
            var self: Self = undefined;
            self.setUsed(len_adjust);

            try self.append(bytes);

            return self;
        }

        pub fn decode(self: Self, out: []u8) Error![]const u8 {
            var offset: usize = 0;

            for (len_adjust..self.packedLen()) |i| {
                if (out.len <= offset)
                    return Error.Overflow;

                const c = self.data.get(i);
                const dec = decco.decode(@intCast(c));
                offset += std.unicode.utf8Encode(dec, out[offset..]) catch unreachable;
            }

            return out[0..offset];
        }

        pub fn append(self: *Self, bytes: []const u8) Error!void {
            var len = self.packedLen();
            var it = std.unicode.Utf8View.initUnchecked(bytes).iterator();

            while (it.nextCodepoint()) |cp| {
                defer len += 1;

                if (len > capacity)
                    return Error.Overflow;

                const enc = decco.encode(cp);
                self.data.set(len, @intCast(enc));
            }

            self.setUsed(len);
        }

        fn packedLen(self: Self) LenT {
            return if (optimize_len) self.data.get(0) else self.len;
        }

        fn setUsed(self: *Self, len: LenT) void {
            if (optimize_len)
                self.data.set(0, len)
            else
                self.len = len;
        }
    };
}

test "smol str ascii_alpha_lower" {
    const orig_str = "halloworld";
    const T = SmolStr(10, codec.AsciiAlphaLower);
    var s = try T.init(orig_str);
    var buf: [10]u8 = undefined;

    const d = try s.decode(&buf);
    try testing.expectEqualSlices(u8, orig_str, d);

    // account for padding between members
    const round_to: usize = 8;
    const expected_bits_round = comptime roundToNextMultOfPow2(
        codec.AsciiAlphaLower.bits() * orig_str.len,
        round_to,
    );
    // len gets optimized away
    try testing.expectEqual(void, @TypeOf(s.len));

    // original: 10 × u8 + 1 × usize = 144
    try testing.expectEqual(56, expected_bits_round);
    try testing.expectEqual(56, @bitSizeOf(T));
}

test "readme example" {
    const capacity = 10;
    const c = codec.AsciiAlphaLower;
    var s = try SmolStr(capacity, c).init("hello");
    try s.append("world");

    // for a runtime slice, it would need 10×u8+1×usize = 144 bits
    try testing.expectEqual(56, @bitSizeOf(@TypeOf(s)));
    var buf: [capacity]u8 = undefined;
    const sl = try s.decode(&buf);
    try testing.expectEqualSlices(u8, "helloworld", sl);
}

test "smol str ascii_alpha_lower extras" {
    const orig_str = "-_,. ";
    const T = SmolStr(10, codec.AsciiAlphaLower);
    var s = try T.init(orig_str);
    var buf: [0x100]u8 = undefined;

    const d = try s.decode(&buf);
    try testing.expectEqualSlices(u8, orig_str, d);
}

test "smol str capacity/len" {
    const orig_str = "halloworldz";
    const T = SmolStr(110, codec.AsciiAlphaLower);
    const s = try T.init(orig_str);

    try testing.expectEqual(orig_str.len, s.len);
}

test "smol str init too long" {
    const orig_str = "halloworldz";
    const T = SmolStr(1, codec.AsciiAlphaLower);
    const s = T.init(orig_str);

    try testing.expectError(error.Overflow, s);
}

test "katakana" {
    const word = "パソコン";
    const s = try SmolStr(4, codec.Katakana).init(word);
    var buf: [0x100]u8 = undefined;
    const d = try s.decode(&buf);
    try testing.expectEqualSlices(u8, word, d);
    try testing.expectEqual(void, @TypeOf(s.len));
    try testing.expectEqual(roundToNextMultOfPow2(5 * 7, @as(usize, 8)), @bitSizeOf(@TypeOf(s)));
}

test "upper ascii" {
    const word = "LOL OMG";
    const s = try SmolStr(7, codec.AsciiAlphaUpper).init(word);
    var buf: [0x100]u8 = undefined;
    const d = try s.decode(&buf);
    try testing.expectEqualSlices(u8, word, d);
}

test "mixed ascii" {
    const word = "LOL omg";
    const s = try SmolStr(7, codec.AsciiAlpha).init(word);
    var buf: [0x100]u8 = undefined;
    const d = try s.decode(&buf);
    try testing.expectEqualSlices(u8, word, d);
}

test "append" {
    var s = try SmolStr(6, codec.AsciiAlpha).init("LOL");
    try s.append("omg");
    var buf: [0x100]u8 = undefined;
    const d = try s.decode(&buf);
    try testing.expectEqualSlices(u8, "LOLomg", d);
}

test "append overflow" {
    var s = try SmolStr(5, codec.AsciiAlpha).init("LOL");
    try testing.expectError(error.Overflow, s.append("omg"));
}

test "to slice overflow" {
    var s = try SmolStr(5, codec.AsciiAlpha).init("abcd");
    var buf: [3]u8 = undefined;
    try testing.expectError(error.Overflow, s.decode(&buf));
}

fn roundToNextMultOfPow2(val: anytype, mult: anytype) @TypeOf(val) {
    return (val + (mult - 1)) & ~(mult - 1);
}

test "round to next multiple of power of 2" {
    try testing.expectEqual(0, roundToNextMultOfPow2(0, @as(usize, 8)));
    try testing.expectEqual(8, roundToNextMultOfPow2(7, @as(usize, 8)));
    try testing.expectEqual(8, roundToNextMultOfPow2(8, @as(usize, 8)));
    try testing.expectEqual(16, roundToNextMultOfPow2(9, @as(usize, 8)));
}
