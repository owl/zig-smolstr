# smolstr
strings what are small.
will eventually support more encodings,
but for now implements smol strings for katakana and ASCII.

is this actually useful? i don't know.

## examples
```zig
const capacity = 10;
const codec = AsciiAlphaLower;
var s = try SmolStr(capacity, codec).init("hello");
try s.append("world");

// for a runtime slice, it would need 10×u8+1×usize = 144 bits
try testing.expectEqual(56, @bitSizeOf(@TypeOf(s)));
// decode it into a buffer
var buf: [10]u8 = undefined;
const sl = try s.decode(&buf);

try testing.expectEqualSlices(u8, "helloworld", sl);
```

```zig
const s = try SmolStr(4, Katakana).init("パソコン");

// would need 12×u8+1×usize = 160 bits
// 20% of original size
try testing.expectEqual(40, @bitSizeOf(@TypeOf(s)));
```

